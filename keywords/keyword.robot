*** Settings ***
Library    Selenium2Library
Variables  C:/Users/ROHITH/PycharmProjects/selenium_robot/variables/variable.yaml
Variables   C:/Users/ROHITH/PycharmProjects/selenium_robot/variables/variable.yaml

*** Keywords ***

Opening_amazon
    [Documentation]    adding a product to cart
    Open Browser    ${url}  chrome
    Wait Until Page Contains   Today's Deals
Search_product
    Input Text      ${element}     laptops
    Click Button    ${search_icon}
    Wait Until Page Contains    results for "laptops"
    Click Element  ${laptop}
    Wait Until Page Contains    Back to results
add_cart
    Click Button    ${addtocart}
    Wait Until Page Contains    Added to Cart

